#include "hardware.h"


static bool 		_startFlag = false;
static uint32_t 	_itrCnt = 0;


int main(void)
{
	GPIO_Init();
	HARDWARE_SysTickInit(100);
	UART_Init(MDR_UART2, 115200 * 2);

	const uint8_t adcDiv = 4;
	ADC_Init(adcDiv);
	
	while(1)
	{
		if(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk)
		{
			const uint8_t adcCh = 6;
			GPIO_TogglePin(LED3_PORT, LED3_PIN_Pos);
			UART_Printf(MDR_UART2, "\r\nTest iteration %u ADC-6 %u", _itrCnt++, ADC_Get(adcCh));
		}

		static bool _btnFlag = false;

		if(!GPIO_ReadPin(BTN_PORT, BTN_PIN_Pos) && !_btnFlag)
		{
			if(!_startFlag)
			{
				_startFlag = true;
				GPIO_WritePin(LED2_PORT, LED2_PIN_Pos, true);
				HARDWARE_ClockInit();
				MDR_RST_CLK->CPU_CLOCK &= ~RST_CLK_CPU_CLOCK_CPU_C2_SEL;
			}
			else
			{	
				GPIO_TogglePin(LED1_PORT, LED1_PIN_Pos);
				GPIO_TogglePin(LED2_PORT, LED2_PIN_Pos);
				MDR_RST_CLK->CPU_CLOCK ^= RST_CLK_CPU_CLOCK_CPU_C2_SEL;
			}
			_btnFlag = true;
		}
		
		if(GPIO_ReadPin(BTN_PORT, BTN_PIN_Pos) && _btnFlag)
		{
			_btnFlag = false;
		}
	}
}

